﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalNumberTest
{
    class Program
    {
        static int totalSum = 0;
        static int digitsSum = 0;
        static int calculatedDigitToAdd = 0;
        public static void Main(string[] args)
        {
            returnResult();
        }

        public static void returnResult()
        {
            int limit = 0;
            Console.WriteLine("Enter the natural number to count (between 1 and 1000)");
            if (int.TryParse(Console.ReadLine(), out limit))
            {
                if (limit > 0 && limit<=1000)
                {
                    List<int> naturalNoList = new List<int>();
                    for (int i = 1; i <= limit; i++)
                    {
                        naturalNoList.Add(i);
                    }

                    foreach (int number in naturalNoList)
                    {
                        multiplyAndAddNumbers(number);
                        totalSum = totalSum + calculatedDigitToAdd;
                        digitsSum = calculatedDigitToAdd = 0;
                    }
                    Console.WriteLine("Total of numbers upto " + limit + " is: " + totalSum);
                    Console.ReadLine();
                }
                else
                {
                    returnResult();
                }
            }
            else
            {
                Console.WriteLine("\nError: Please enter positive number only.\n");
                returnResult();
            }
        }

        public static void multiplyAndAddNumbers(int numberToCalculate)
        {
            int digitsMultiplication = 1;
            var digitsArray = getAllDigitsOfNumber(numberToCalculate);
            if (digitsArray.Length > 1)
            {
                foreach (var digit in digitsArray)
                {
                    if (digit != 0)
                        digitsMultiplication *= digit;
                }
                if (getAllDigitsOfNumber(digitsMultiplication).Length > 1)
                    multiplyAndAddNumbers(digitsMultiplication);
                else
                    calculatedDigitToAdd = digitsMultiplication;
            }
            else
            {
                calculatedDigitToAdd = numberToCalculate;
            }
        }

        public static int[] getAllDigitsOfNumber(int value)
        {
            int length = Convert.ToString(value).Length;
            int[] digitsArray = new int[length];
            for (int i = length - 1; i >= 0; i--)
            {
                digitsArray[i] = value % 10;
                value = value / 10;
            }
            return digitsArray;
        }
    }
}
